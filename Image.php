<?php

class Image {

  #Check type of image
  public function checkImageType($extension){
    try {
        $arr = array('png','gif','jpg','jpeg');

        if (!in_array($extension, $arr)) {
          return false;
        }
        return true;
    } catch (Exception $e) {
      echo "Internal exception";
    }
  }

  #Convert base64 code to file and save to Directory
  public function convertBase64Image($base64, $orginal_file_name) {
    try {

        #Delete 'data:image/png;base64,' in base64
        list(, $base64) = explode(';', $base64);
        list(, $base64) = explode(',', $base64);

        $base64 = base64_decode($base64);

        #Save image
        file_put_contents($orginal_file_name, $base64);
        chmod($orginal_file_name, 0777);

        return $orginal_file_name;
    } catch (Exception $e) {
        echo "Internal exception";
    }

  }
  #check image that exist in server
  public function checkFileExist($file_name) {
    try {
    $array_file = glob("$file_name*.*");
    if (count($array_file) > 0) {
       unlink($array_file[0]);
    }
  } catch (Exception $e) {
    echo "Internal exception";
  }
  }

  public function checkAndCreateDir($dirArray) {
    try {
      foreach ($dirArray as $value) {
        if (!file_exists($value)) {
          mkdir($value, 0777, true);
          chmod($value,0777);
        } else {
          array_map('unlink', glob($value."*.*"));
        }
      }
    } catch (Exception $e) {
      echo "Internal exception";
    }
  }

  function getFileSizeInKb($base64string){
    try {
      $bytes = strlen(base64_decode($base64string));
      $roughsize = (((int)$bytes) / 1024.0);
      return $roughsize; //round($roughsize,2);
    } catch (Exception $e) {
      echo "Internal exception";
    }
  }

  public function cropImageBySize($filename, $newWith, $newHeight, $fullPath, $extension) {
    try {
        list($width, $height) = getimagesize($filename);
        $newImage = imagecreatetruecolor($newWith, $newHeight);

        if ($extension == 'jpeg' || $extension == 'jpg') {
          $source = imagecreatefromjpeg($filename);
          imagecopyresized($newImage, $source, 0, 0, 0, 0, $newWith, $newHeight, $width, $height);
            if(imagejpeg($newImage, $fullPath, 100))
              return $fullPath;
        } elseif ($extension == 'png') {
            $source = imagecreatefrompng($filename);
            imagecopyresized($newImage, $source, 0, 0, 0, 0, $newWith, $newHeight, $width, $height);
            if(imagepng($newImage, $fullPath, 9/100))
              return $fullPath;
        } elseif ($extension == 'gif') {
            $source = imagecreatefromgif($filename);
            imagecopyresized($newImage, $source, 0, 0, 0, 0, $newWith, $newHeight, $width, $height);
            if(imagegif($newImage, $fullPath))
              return $fullPath;
        } else throw new Exception('The extension is not correct');
    } catch (Exception $e) {
      echo "Internal exception";
      }
  }
  function deleteDirectory($dirPath) {
    try {
        if (is_dir($dirPath)) {
            $objects = scandir($dirPath);
            foreach ($objects as $object) {
                if ($object != "." && $object !="..") {
                    if (filetype($dirPath . DIRECTORY_SEPARATOR . $object) == "dir") {
                        $image = new Image();
                        $image->deleteDirectory($dirPath . DIRECTORY_SEPARATOR . $object);
                    } else {
                        unlink($dirPath . DIRECTORY_SEPARATOR . $object);
                    }
                }
            }
        reset($objects);
        rmdir($dirPath);
        }
    } catch (Exception $e) {
        echo "Internal exception";
      }
  }

}
 ?>
