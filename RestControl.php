<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Content-Type");
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("ImageRestHandler.php");
require_once('vendor/autoload.php');
use \Firebase\JWT\JWT;

$REQ_DATA = file_get_contents('php://input');
$REQ_DATA = json_decode($REQ_DATA);
if (isset($_POST["action"])) {
  $action = $_POST["action"];
  $userId = $_POST["userId"];
  $token = $_POST["token"];
} elseif (isset($REQ_DATA)) {
  $action = $REQ_DATA->action;
  $userId = $REQ_DATA->userId;
  $token = $REQ_DATA->token;
} else {
  throw new Exception('Invalid parameter');
}
// } elseif (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"])) {
//   $action = $_REQUEST["action"];
// }
session_id($userId);
session_start();

switch ($action) {
  case 'upload':

    $jwt = JWT::decode($token, 'TokenUnique', array('HS256'));

    if ($_SESSION[$userId]->userId == $jwt->userId) {
      $ImageRestHandler = new ImageRestHandler();
      $ImageRestHandler->uploadImage();
    } else {
      throw new Exception("Permission deny");
    }

    break;
  case 'delete':
    $jwt = JWT::decode($token, 'TokenUnique', array('HS256'));

    if ($_SESSION[$userId]->userId == $jwt->userId) {
      $ImageRestHandler = new ImageRestHandler();
      $ImageRestHandler->deleteImage();
    } else {
      throw new Exception("Permission deny");
    }

    break;
  case 'updateToken':
    $jwt = JWT::decode($token, 'TokenUnique', array('HS256'));
    $_SESSION[$userId] = $jwt;

    break;
  case 'deleteToken':
    $jwt = JWT::decode($token, 'TokenUnique', array('HS256'));

    if ($_SESSION[$userId]->userId == $jwt->userId) {
      session_destroy();
    } else {
      throw new Exception("Permission deny");
    }
}
?>
