<?php

require_once("SimpleRest.php");
require_once("Image.php");

class ImageRestHandler extends SimpleRest {

    function uploadImage() {

        $image = new Image();

        try {
        # Check validation params in post method
          if (!isset($_POST['userId']) ||
              !isset($_POST['fileObject']) ||
              !isset($_POST['fileUpload']))
          {
            throw new Exception("Invalid parameter");
          }

        #Get params in post method
        $userId = $_POST['userId'];
        #base64 of picture
        $base64 = $_POST['fileUpload'];
        $fileObject = $_POST['fileObject'];

        if ($image->checkImageType($fileObject['extension']) == false) {
          throw new Exception("Invalid type image");
        }

        $size = $image->getFileSizeInKb($base64);

        $file_dir = 'uploads/'.$userId;
        $dirArray = [];
        if ($fileObject['type'] === 'image') {
          if ($size > 3072) {
           throw new Exception("Please upload file size smaller 3mb");
          }

          $file_newDir = $file_dir.'/image';
          $dir_170 = $file_newDir."/170/";
          $dir_50 = $file_newDir."/50/";
          array_push($dirArray, $dir_170, $dir_50);
          $dir_orginials = $file_newDir.'/original/';
        }elseif ($fileObject['type'] === 'background'){
          if ($size > 10240) {
           throw new Exeption("Please upload file size smaller 10mb");
          }

          $file_newDir = $file_dir.'/background/';
          $dir_2600 = $file_newDir."/2600/";
          array_push($dirArray, $dir_2600);
          $dir_orginials = $file_newDir.'original/';
        }
        array_push($dirArray, $dir_orginials);
        $image->checkAndCreateDir($dirArray);
        $orginal_file_name = $dir_orginials . $userId. '.' . $fileObject['extension'];
        $orginal_file_name = $image->convertBase64Image($base64, $orginal_file_name);

        $response = array(
          "statusCode" => 200
          //"urlFile" => $userId. '.' . $fileObject['extension']
        );
        if ($fileObject['type'] === 'image'){
            $fullPath170 = $dir_170 . $userId . '.' . $fileObject['extension'];
            $fullPath50 = $dir_50 . $userId . '.' . $fileObject['extension'];
            $image->cropImageBySize($orginal_file_name, 170, 170, $fullPath170, $fileObject['extension']);
            $image->cropImageBySize($orginal_file_name, 50, 50, $fullPath50, $fileObject['extension']);
        } elseif ($fileObject['type'] === 'background'){
          $fullPathBackground = $file_newDir . '2600/'.$userId . '.' . $fileObject['extension'];
          list($oldWidth, $oldHeight) = getimagesize($orginal_file_name);
            if ($oldWidth != 2600) {
              $ratio = $oldWidth / 2600;
              $newheight = $oldHeight / $ratio;
            }
          $image->cropImageBySize($orginal_file_name, 2600, $newheight, $fullPathBackground, $fileObject['extension']);
        }

        echo json_encode($response);
      } catch (Exception $e) {
          echo $e -> getMessage();
        }
    }

    #Delete image function
    public function deleteImage() {
      try {
          if (!isset($_POST["userId"]) ||
              !isset($_POST["typeImage"])) {
              throw new Exception("Invalid parameters");
          }
          $response = array(
            "statusCode" => 200

          );
          $userId = $_POST["userId"];
          $type_image = $_POST["typeImage"];

          $file_dir = 'uploads/'.$userId.'/'.$type_image;
          $image = new Image();
          $image->deleteDirectory($file_dir);

          echo json_encode($response);

      } catch (Exception $e) {
        echo $e -> getMessage();
      }
    }
  }
 ?>
